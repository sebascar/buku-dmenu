# buku-dmenu

A simple bash dmenu wrapper for the buku bookmark manager.

Requires:

- bash
- [buku](https://github.com/jarun/Buku)
- sed, cut
- [dmenu](http://tools.suckless.org/dmenu/)
- xargs

## Installation

- Download the script.
- Make sure the script is executable `chmod +x buku-dmenu`
- Then just stick it anywhere you like, probably in your $PATH

### Arch Linux

Arch users can get the [buku-dmenu](https://aur.archlinux.org/packages/buku-dmenu/) from the AUR.

## Usage

Just run the script:

    ./buku-dmenu

You then whittle down your choice in dmenu and hit enter to open the bookmark in your web browser.

dmenu is set to be case-insensitive in this script.

The information passed to dmenu is formatted as such:

    ID NameOfBookmark

So for instance

    34 BBC News

You can search on any of these terms, I find things got really fast once I started remembering the IDs of my favourites. To get to the above example you just type `34` and hit Enter.

## To-Do list

If there is enough interest it could be worth reading some options from a config file.

This would mainly be to change the appearance and behaviour of dmenu, which is currently hardcoded. This could include things like:

- dmenu position
- font
- colours

If I get time I would like to redesign this so that it doesn't have to hit buku twice. I quite like the simplicity of it right now though.
